/**
 * @author Laura Castro <lcastro@udc.es>
 * @copyright (C) 2014
 * @doc Sample file: simple object to add integers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 */
public class Adder {
	
    /**
     * Constructor.
     */	
    public Adder() {
	_value = 0;
    }

    /**
     * Adds an integer to the value held by the Adder object.
     * @param n Value to be added
     * @return Updated value held by the Adder object.
     */
    public Integer add(Integer n) {
	_value += n.intValue();

	return new Integer(_value);
    }


    // Private attributes
    private int _value;
}
