/**
 * @author Laura Castro <lcastro@udc.es>
 * @copyright (C) 2014
 * @doc Sample JUnit file: tests for the simple object to add integers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 */
import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.*;

public class AdderTest {

    @Before
    public void setUp() {
	_adder = new Adder();
    }

    @Test
    public void testScenario() {
	assertEquals(new Integer(  0), _adder.add(0));
	assertEquals(new Integer(  1), _adder.add(1));
	assertEquals(new Integer( 11), _adder.add(10));
	assertEquals(new Integer(  6), _adder.add(-5));
	assertEquals(new Integer( -5), _adder.add(-11));
	assertEquals(new Integer(-10), _adder.add(_adder.add(0)));
    }

    // Test attributes
    private Adder _adder;

}
