%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro <lcastro@udc.es>
%%% @copyright (C) 2014
%%% @doc Sample PROPER spec: PB-tests for simple server to add integers.
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% @end
%%%-------------------------------------------------------------------
-module(adder_eqc).

-include_lib("eqc/include/eqc.hrl").
-include_lib("eqc/include/eqc_statem.hrl").

-compile(export_all).

%%--------------------------------------------------------------------
%% @doc Returns the state in which each test case starts. (Unless a different 
%%      initial state is supplied explicitly to, e.g. commands/2.)
%% @end
%%--------------------------------------------------------------------
-spec initial_state() -> eqc_statem:symbolic_state().
initial_state() ->
    {stopped, 0}.

%% ------ Common pre-/post-conditions --------------------------------
%% @doc General command filter, checked before a command is generated.
%% @end
%%--------------------------------------------------------------------
-spec command_precondition_common(S :: eqc_statem:symbolic_state(),
				  Command :: atom()) -> boolean().
command_precondition_common(_S, _Command) ->
    true.

%%--------------------------------------------------------------------
%% @doc General precondition, applied *before* specialized preconditions.
%% @end
%%--------------------------------------------------------------------
-spec precondition_common(S :: eqc_statem:symbolic_state(), 
			  C :: eqc_statem:call()) -> boolean().
precondition_common(_S, _Call) ->
    true.

%%--------------------------------------------------------------------
%% @doc General postcondition, applied *after* specialized postconditions.
%% @end
%%--------------------------------------------------------------------
-spec postcondition_common(S :: eqc_statem:dynamic_state(), 
			   C :: eqc_statem:call(), Res :: term()) -> boolean().
postcondition_common(_S, _Call, _Res) ->
    true.

%% ------ Grouped operator: start ------------------------------------
%% @doc start_command - Command generator
%%--------------------------------------------------------------------
-spec start_command(S :: eqc_statem:symbolic_state()) -> 
			   eqc_gen:gen(eqc_statem:call()).
start_command(_S) -> 
    {call, adder, start, []}.

%%--------------------------------------------------------------------
%% @doc start_pre - Precondition for generation
%% @end
%%--------------------------------------------------------------------
-spec start_pre(S :: eqc_statem:symbolic_state()) -> boolean().
start_pre({stopped, _S}) ->
    true;
start_pre(_) ->
    false.

%%--------------------------------------------------------------------
%% @doc start_pre - Precondition for start
%% @end
%%--------------------------------------------------------------------
-spec start_pre(S :: eqc_statem:symbolic_state(), 
		Args :: [term()]) -> boolean().
start_pre(_S, []) ->
    true. %% Condition for S + Args

%%--------------------------------------------------------------------
%% @doc start_next - Next state function
%% @end
%%--------------------------------------------------------------------
-spec start_next(S :: eqc_statem:symbolic_state(), 
		 V :: eqc_statem:var(), 
		 Args :: [term()]) -> eqc_statem:symbolic_state().
start_next({stopped, _S}, _Value, []) ->
    {started, 0}.

%%--------------------------------------------------------------------
%% @doc start_post - Postcondition for start
%% @end
%%--------------------------------------------------------------------
-spec start_post(S :: eqc_statem:dynamic_state(), 
		 Args :: [term()], R :: term()) -> true | term().
start_post({stopped, _S}, [], Res) ->
    Res == started.

%% ------ Grouped operator: stop -------------------------------------
%% @doc stop_command - Command generator
%%--------------------------------------------------------------------
-spec stop_command(S :: eqc_statem:symbolic_state()) -> 
			  eqc_gen:gen(eqc_statem:call()).
stop_command(_S) -> 
    {call, adder, stop, []}.

%%--------------------------------------------------------------------
%% @doc stop_pre - Precondition for generation
%% @end
%%--------------------------------------------------------------------
-spec stop_pre(S :: eqc_statem:symbolic_state()) -> boolean().
stop_pre({started, _S}) ->
    true;
stop_pre(_) ->
    false.

%%--------------------------------------------------------------------
%% @doc stop_pre - Precondition for stop
%% @end
%%--------------------------------------------------------------------
-spec stop_pre(S :: eqc_statem:symbolic_state(), 
	       Args :: [term()]) -> boolean().
stop_pre(_S, []) ->
    true. %% Condition for S + Args

%%--------------------------------------------------------------------
%% @doc stop_next - Next state function
%% @end
%%--------------------------------------------------------------------
-spec stop_next(S :: eqc_statem:symbolic_state(), 
		V :: eqc_statem:var(), 
		Args :: [term()]) -> eqc_statem:symbolic_state().
stop_next({started, _S}, _Value, []) ->
    {stoped, 0}.

%%--------------------------------------------------------------------
%% @doc stop_post - Postcondition for stop
%% @end
%%--------------------------------------------------------------------
-spec stop_post(S :: eqc_statem:dynamic_state(), 
		Args :: [term()], R :: term()) -> true | term().
stop_post({started, _S}, [], Res) ->
    Res == stopped.

%% ------ Grouped operator: add --------------------------------------
%% @doc add_command - Command generator
%% @end
%%--------------------------------------------------------------------
-spec add_command(S :: eqc_statem:symbolic_state()) -> 
			 eqc_gen:gen(eqc_statem:call()).
add_command(_S) -> 
    {call, adder, add, [int()]}.

%%--------------------------------------------------------------------
%% @doc add_pre - Precondition for generation
%% @end
%%--------------------------------------------------------------------
-spec add_pre(S :: eqc_statem:symbolic_state()) -> boolean().
add_pre({started, _S}) ->
    true;
add_pre(_) ->
    false.

%%--------------------------------------------------------------------
%% @doc add_pre - Precondition for add
%% @end
%%--------------------------------------------------------------------
-spec add_pre(S :: eqc_statem:symbolic_state(), 
	      Args :: [term()]) -> boolean().
add_pre(_S, [_N]) ->
    true. %% Condition for S + Args

%%--------------------------------------------------------------------
%% @doc add_next - Next state function
%% @end
%%--------------------------------------------------------------------
-spec add_next(S :: eqc_statem:symbolic_state(), 
	       V :: eqc_statem:var(), 
	       Args :: [term()]) -> eqc_statem:symbolic_state().
add_next({started, S}, _V, [N]) ->
    {started, S+N}.

%%--------------------------------------------------------------------
%% @doc add_post - Postcondition for add
%% @end
%%--------------------------------------------------------------------
-spec add_post(S :: eqc_statem:dynamic_state(), 
		 Args :: [term()], R :: term()) -> true | term().
add_post({started, S}, [N], Res) ->
    Res == (S+N).

%%--------------------------------------------------------------------
%% @doc weight/2 - Distribution of calls
%% @end
%%--------------------------------------------------------------------
-spec weight(S :: eqc_statem:symbolic_state(), Command :: atom()) -> integer().
weight(_S, start) -> 20;
weight(_S, stop)  -> 20;
weight(_S, add)   -> 60.

%%--------------------------------------------------------------------
%% @doc Default generated property
%% @end
%%--------------------------------------------------------------------
-spec prop_adder() -> eqc:property().
prop_adder() ->
    ?FORALL(Cmds, commands(?MODULE),
	    begin
		{H, S, Res} = run_commands(?MODULE,Cmds),
		cleanup(),
		pretty_commands(?MODULE, Cmds, {H, S, Res},
				Res == ok)
	    end).

%% Internal wrappers and auxiliary functions
cleanup() ->
    catch adder:stop().
