%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @copyright 2014, Laura M. Castro
%%% @doc `gamifiTEST': coverage wrapper.
%%%      Gamification of unit testing and property-based testing
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see [http://www.gnu.org/licenses].
%%% @see cover
%%% @end
%%%-------------------------------------------------------------------
-module(coverage_wrapper).
-behaviour(test_wrapper).

-include("gamifiTEST.hrl").

%% PUBLIC API
-export([process/1]).

%%-------------------------------------------------------------------
%% @doc Wrapper for Cover/Smother.
%%      Receives a module name (module needs not be compiled).
%%      Tool selection is enabled via the COVERAGE_TOOL macro
%%      defined in the `gamifiTEST.hrl' file.
%%      To calculate coverage, all existing eUnit and PBT tests are
%%      used, so corresponding test files should be compiled and
%%      follow the expected naming conventions.
%% @end
%%-------------------------------------------------------------------
-spec process(Module :: atom()) -> {ok, term()} | {error, term()}.
process(Module) when is_atom(Module) ->
    {module, Module} = code:ensure_loaded(Module),
    ?COVERAGE_TOOL:compile(code:where_is_file(atom_to_list(Module) ++ ".erl")),
    eunit_wrapper:process(Module),
    pbt_wrapper:do_process(Module, ?PBT_TOOL),
    {ok, Data} = ?COVERAGE_TOOL:analyse(Module, calls),
    {ok, MoreData} = ?COVERAGE_TOOL:analyse(Module),
    {cover, Data ++ MoreData}.
    
