/**
 * @author Laura Castro <lcastro@udc.es>
 * @copyright (C) 2014
 * @doc gamifiTEST: JUnitWrapper
 *      Gamification of unit testing and property-based testing.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 */
import org.junit.runner.JUnitCore;
import org.junit.runner.Request;
import org.junit.runner.Result;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;
import java.util.Stack;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangInt;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.ericsson.otp.erlang.OtpNode;
import com.ericsson.otp.erlang.OtpMbox;

public class JUnitWrapper {

    public static void main(java.lang.String... args) throws ClassNotFoundException,
							     IOException {

	Properties props = new Properties();

	int failed  = 0;
	int skipped = 0;
	int total   = 0;
	long elapsedTime = 0;

	for (String s : args) {
	    // debug("Testing " + s + "\n");
	    // debug("Current ClassLoader chain: " + getCurrentClassloaderDetail());
	    JUnitCore junit = new JUnitCore();
	    Result r = junit.run(Request.aClass(Class.forName(s)));
	    debug("Results: " + r.getFailureCount() + " failed, "
		              + r.getIgnoreCount()  + " skipped, "
		              + r.getRunCount()     + " run, "
		              + r.getRunTime()      + " (ms) elapsed time.\n");
	    failed  += r.getFailureCount();
	    skipped += r.getIgnoreCount();
	    total   += r.getRunCount();
	    elapsedTime += r.getRunTime();
	}

	props.load(readProperties());

	OtpNode node = new OtpNode(props.getProperty(SENDER_NODE),
				   props.getProperty(SENDER_COOKIE));
	OtpMbox mbox = node.createMbox(props.getProperty(SENDER_NODE)); 

	if (node.ping(props.getProperty(RECEIVER_NODE), TIMEOUT)) {
	    debug("Remote node " + props.getProperty(RECEIVER_NODE) + " is up => reporting.\n");

	    OtpErlangObject[] msg    = new OtpErlangObject[2];
	    OtpErlangObject[] values = new OtpErlangObject[4];

	    OtpErlangObject[] f = new OtpErlangObject[2];
	    OtpErlangObject[] s = new OtpErlangObject[2];
	    OtpErlangObject[] t = new OtpErlangObject[2];
	    OtpErlangObject[] e = new OtpErlangObject[2];

	    f[0] = new OtpErlangAtom("failed");
	    f[1] = new OtpErlangInt(failed);
	    values[0] = new OtpErlangTuple(f);

	    s[0] = new OtpErlangAtom("skipped");
	    s[1] = new OtpErlangInt(skipped);
	    values[1] = new OtpErlangTuple(s);

	    t[0] = new OtpErlangAtom("run");
	    t[1] = new OtpErlangInt(total);
	    values[2] = new OtpErlangTuple(t);

	    e[0] = new OtpErlangAtom("elapsed");
	    e[1] = new OtpErlangLong(elapsedTime);
	    values[3] = new OtpErlangTuple(e);

	    msg[0] = new OtpErlangAtom(props.getProperty(RECEIVER_TOOL));
	    msg[1] = new OtpErlangList(values);
	    OtpErlangTuple tuple = new OtpErlangTuple(msg);

	    mbox.send(props.getProperty(RECEIVER_PROCESS),
		      props.getProperty(RECEIVER_NODE), tuple);

        } else {
	    debug("Remote node " + props.getProperty(RECEIVER_NODE) + " is not up => not reporting.\n");
	}

    }

    private static void debug(String dbgStr) {
	stream.print(DEBUG ? "[DEBUG] =====> " + dbgStr : "");
    }

    private static InputStream readProperties() {
	return JUnitWrapper.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE);
    }

    private static String getCurrentClassloaderDetail() {
              
	StringBuffer classLoaderDetail = new StringBuffer();       
	Stack<ClassLoader> classLoaderStack = new Stack<ClassLoader>();
              
	ClassLoader currentClassLoader = Thread.currentThread().getContextClassLoader();
              
	classLoaderDetail.append("\n--------------------------------------------------\n");
              
	// Build a Stack of the current ClassLoader chain
	while (currentClassLoader != null) {
	    classLoaderStack.push(currentClassLoader);
	    currentClassLoader = currentClassLoader.getParent();
	}
              
	// Print ClassLoader parent chain
	while(classLoaderStack.size() > 0) {
	    ClassLoader classLoader = classLoaderStack.pop();
                      
	    // Print current                     
	    classLoaderDetail.append(classLoader);
            
	    if (classLoaderStack.size() > 0) {
		classLoaderDetail.append("\n--- delegation ---\n");                               
	    } else {
		classLoaderDetail.append(" **Current ClassLoader**");
	    }
	}
        
	classLoaderDetail.append("\n--------------------------------------------------\n");
        
	return classLoaderDetail.toString();
    }

    private static final boolean DEBUG = true;
    private static final int TIMEOUT  = 2000;

    private static final PrintStream stream = System.err;

    private static final String SENDER_NODE    = "sender.node";
    private static final String SENDER_COOKIE  = "sender.cookie";
    private static final String RECEIVER_NODE    = "receiver.node";
    private static final String RECEIVER_PROCESS = "receiver.process";
    private static final String RECEIVER_TOOL    = "receiver.tool";

    private static final String PROPERTIES_FILE = "config.properties";

}
