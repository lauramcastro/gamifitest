%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @copyright 2014, Laura M. Castro
%%% @doc `gamifiTEST_io'
%%%      Gamification of unit testing and property-based testing:
%%%      Module to send behaviours and achievements to gamification server
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see [http://www.gnu.org/licenses].
%%% @end
%%%-------------------------------------------------------------------
-module(gamifiTEST_io).

%% PUBLIC API
-export([request/0, report/1]).

%%-------------------------------------------------------------------
%% @doc Requests info from gamification server.
%%      To help other parts of the gamifiTEST application to decide
%%      whether game achievements have been made after test execution,
%%      this method requests updated information from the gamification
%%      server.
%% @end
%%-------------------------------------------------------------------
-spec request() -> Data :: term().
request() ->
    io:format("[~p] requesting data~n", [?MODULE]),
    % TODO: when the REST server is up & running.
    % Use restc client:
    % restc:request(get,
    %               json,
    %               "http://http://lauracastro.cli.lbd.org.es:80/framework/api/users",
    %               [200],
    %               [{"ToolCode", "APIDOC"}, {"ToolPassword", "apidoc"}]).
    [].

%%-------------------------------------------------------------------
%% @doc Sends info to gamification server.
%%      Once other components of the gamifiTEST application have
%%      determined updates to the data held by the gamification
%%      server, this is the callback to propagate those.
%% @end
%%-------------------------------------------------------------------
-spec report(Data :: term()) -> ok.
report(Data) ->
    io:format("[~p] reporting data: ~p~n", [?MODULE, Data]),
    % TODO: when the REST server is up & running.
    % Use restc client:
    % restc:request(get, "https://api.github.com").
    % restc:request(get, "https://api.github.com", [200]).
    % restc:construct_url("http://www.example.com/te", "res/res1/res2", [{"q1", "qval1"}, {"q2", "qval2"}]).
    ok.
