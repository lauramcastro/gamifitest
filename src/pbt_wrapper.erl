%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @copyright 2014, Laura M. Castro
%%% @doc `gamifiTEST': PBT wrapper.
%%%      Gamification of unit testing and property-based testing
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see [http://www.gnu.org/licenses].
%%% @end
%%%-------------------------------------------------------------------
-module(pbt_wrapper).
-behaviour(test_wrapper).

-include("gamifiTEST.hrl").

%% PUBLIC API (behaviour)
-export([process/1]).
%% INTERNAL API
-export([do_process/2]).

%% @doc Wrapper for QuickCheck/Proper.
%%      Receives a test module name (test module must be compiled).
%%      Tool selection is enabled via the PBT_TOOL macro
%%      defined in the `gamifiTEST.hrl' file. It acts as a proxy of
%%      the corresponding PBT tool, delegating the
%%      actual execution of the tests, and listens for the statistical
%%      info.
%% @end
-spec process(Module :: atom()) -> term().
process(Module) when is_atom(Module) ->
    do_process(Module, ?PBT_TOOL).

% @private
do_process(Module, PBTtool) ->
    PBTModule = pre_process(Module, PBTtool),
    ModuleExports = PBTModule:module_info(exports),
    PBTProps = [ Name || {Name, 0} <- ModuleExports, is_property(Name)],
    register(?PBT_DATA_RECEIVER, self()),
    register(?PBT_DATA_SENDER, spawn(fun loop/0)),
    ok = post_process(PBTtool, PBTModule, PBTProps),
    ?PBT_DATA_SENDER ! stop,
    receive
	{?PBT_DATA_SENDER, TestResults} ->
	    unregister(?PBT_DATA_RECEIVER),
	    {?PBT_TOOL, TestResults}
    end.

pre_process(Module, eqc) when is_atom(Module) ->
    add_suffix(Module, "_eqc");
pre_process(Module, proper) when is_atom(Module) ->
    add_suffix(Module, "_proper").

post_process(_, _, []) ->
    ok;
post_process(eqc, Module, [Prop | MoreProps]) ->
    ?PBT_TOOL:quickcheck(?PBT_TOOL:on_test(fun account/2, Module:Prop())),
    post_process(eqc, Module, MoreProps);
post_process(proper, Module, [Prop | MoreProps]) ->
    ?PBT_TOOL:quickcheck(Module:Prop()), % TODO: find a way to collect info from PropEr
    post_process(proper, Module, MoreProps),
    ok.



%% Internal stuff

-spec add_suffix(Filename :: atom(), Suffix :: list()) -> atom().
add_suffix(Filename, Suffix) when is_atom(Filename), is_list(Suffix) ->
    list_to_atom(atom_to_list(Filename) ++ Suffix).

-spec is_property(Name :: atom()) -> boolean().
is_property(Name) when is_atom(Name) ->
    lists:prefix("prop_", atom_to_list(Name)).

% TODO: from here downwards to be refactored into pbt_collect module
-spec account(TestCase :: term(), B :: boolean()) -> ok.
account(_TestCase, true) ->
    ?PBT_DATA_SENDER ! {?PBT_DATA_RECEIVER, positive};
account(_TestCase, false) ->
    ?PBT_DATA_SENDER ! {?PBT_DATA_RECEIVER, negative}.

loop() ->
    loop({0, 0, 0, erlang:now()}).

loop({NT, P, N, T}) ->
    receive
	{?PBT_DATA_RECEIVER, positive} ->
	    loop({NT+1, P+1, N, T});
	{?PBT_DATA_RECEIVER, negative} ->
	    loop({NT+1, P, N+1, T});
	stop ->
	    ?PBT_DATA_RECEIVER ! {?PBT_DATA_SENDER, [{failed, N},
						     {succeeded, P},
						     {run, NT},
						     {elapsed, to_seconds(timer:now_diff(erlang:now(), T))}]},
	    unregister(?PBT_DATA_SENDER)
    end.

to_seconds(T) ->
    T / 1000000.0.
