%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @copyright (C) 2014, Laura M. Castro
%%% @doc `gamifiTEST': `eunit_collect'.
%%%      Defines an `eunit_listener' to collect info from test runs.
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see [http://www.gnu.org/licenses].
%%% @see eunit
%%% @end
%%%-------------------------------------------------------------------
-module(eunit_collect).
-behaviour(eunit_listener).

-include("gamifiTEST.hrl").

-include_lib("eunit/include/eunit.hrl").
%-include("eunit/include/eunit_internal.hrl").

%% PUBLIC API (behaviour)
-export([start/0, start/1]).
-export([init/1, handle_begin/3, handle_end/3, handle_cancel/3, terminate/2]).

%% STATE RECORD (for internal use) (TODO: move to gamifiTEST.hrl and use in pbt_wrapper)
-record(state, {numtests = 0,
		passed = 0,
		failed = 0,
		elapsed_time = 0.0}). % in seconds

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ===================================================================
%% eunit_collect API
%% ===================================================================

% @doc Starts `eunit_collect' eUnit listener.
% @end
% @see start/1
start() ->
    start([]).

% @doc Starts `eunit_collect' eUnit listener.
%      Receives a list of initialization options. This callback is
%      included to comply with the interface of the behaviour
%      `eunit_listener'.
% @end
start(Options) ->
    eunit_listener:start(?MODULE, Options).

%% ===================================================================
%% eunit_listener callbacks
%% ===================================================================

% @private
init(_Options) ->
    receive
	{start, _Reference} ->
	    #state{}
    end.

% @private
terminate({ok, Data}, State) ->
    Passed = proplists:get_value(pass, Data),
    Failed = proplists:get_value(fail, Data),
    UpdatedState = State#state{numtests = Passed + Failed,
			       passed = Passed,
			       failed = Failed},
    TestData = [{failed,  UpdatedState#state.failed},
		{run,     UpdatedState#state.numtests},
		{elapsed, UpdatedState#state.elapsed_time}],
    ?UNIT_DATA_RECEIVER ! {?UNIT_DATA_SENDER, TestData},
    ok;
terminate({error, _Reason}, _State) ->
    ok.

% @private
handle_begin(Kind, _Data, State) when Kind == group; Kind == test ->
    %% NOTHING TO DO HERE
    % io:format("[~p] handle_begin ~p~n", [?MODULE, Data]),
    State.

% @private
handle_end(Kind, Data, State) when Kind == group ; Kind == test ->
    ElapsedTime = proplists:get_value(time, Data),
    UpdatedState = State#state{elapsed_time = State#state.elapsed_time + ElapsedTime},
    UpdatedState.

% @private
% Cancel group does not give information on the individual cancelled test case
% We ignore this event...
handle_cancel(Kind, _Data, State) when Kind == group; Kind == test ->
    %% NOTHING TO DO HERE
    % io:format("[~p] handle_cancel ~p~n", [?MODULE, Data]),
    State.

