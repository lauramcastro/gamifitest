%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @copyright 2014, Laura M. Castro
%%% @doc `gamifiTEST'
%%%      Gamification of unit testing and property-based testing
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see [http://www.gnu.org/licenses].
%%% @end
%%%-------------------------------------------------------------------
-module(gamifiTEST_daemon).

-behaviour(gen_server).

-include("gamifiTEST.hrl").

%% Public API
-export([start_link/0, report/1]).

%% Behaviour gen_server callbacks
-export([init/1,
	 handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-record(state, {log}).

%% ===================================================================
%% Gen_server API
%% ===================================================================

%-------------------------------------------------------------------
% @doc Starts the `gamifiTEST' daemon server.
%      The `gamifiTEST' daemon server is a server process that
%      listens for notifications of test activities outside the
%      Erlang VM. It is, hence, the core of integration activities
%      with test tools in/for other languages, such as JUnit, for
%      instance. This callback is mandatory, following the Erlang/OTP
%      behaviour gen_server.
% @end
%-------------------------------------------------------------------
-spec start_link() -> {ok, pid()} | ignore | {error, Reason :: string()}.
start_link() ->
    gen_server:start_link({local, ?EXTERNAL_DATA_RECEIVER}, ?MODULE, [], []).

%-------------------------------------------------------------------
% @doc Receives information reports.
%      This is the callback to be invoked during the execution
%      lifespan of the `gamifiTEST' daemon server, in order to
%      submit external test activity data.
% @end
%-------------------------------------------------------------------
report(Msg) ->
    gen_server:cast(?EXTERNAL_DATA_RECEIVER, {report, Msg}).



%% ===================================================================
%% Gen_server callbacks
%% ===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc Initializes the server
%% @end
%%--------------------------------------------------------------------
-spec init(Args :: list()) -> {ok, State :: record()}.
init([]) ->
    {ok, Fd} = file:open(?LOG_FILE, [read, append]),
    {ok, #state{log = Fd}}.

%%--------------------------------------------------------------------
%% @private
%% @doc Handling call messages
%% @end
%%--------------------------------------------------------------------
-spec handle_call(Request :: any(), From :: pid(), State :: record())
		 -> {noreply, State :: record()}.
handle_call(_Whatever, _From, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc Handling cast messages
%% @end
%%--------------------------------------------------------------------
-spec handle_cast(Msg :: any(), State :: record())
		 -> {noreply, State :: record()}.
handle_cast({report, Msg}, State) ->
    io:format(State#state.log, "====> received ~p~n", [Msg]),
    {noreply, State};
handle_cast(_Whatever, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc Handling all non call/cast messages
%% @end
%%--------------------------------------------------------------------
-spec handle_info(Info :: any(), State :: record())
		 -> {noreply, State :: record()}.
handle_info(Info = {Tool, Data}, State) ->
    io:format(State#state.log, "[~p] Processing data: ~p from tool ~p~n", [?EXTERNAL_DATA_RECEIVER, Data, Tool]),
    gamifiTEST:measure_and_report(Info),
    {noreply, State};
handle_info(Info, State) ->
    io:format(State#state.log, "[~p] Unprocessed data from unknown tool ~p~n", [?EXTERNAL_DATA_RECEIVER, Info]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc This function is called by a gen_server when it is about to
%%      terminate. It should be the opposite of Module:init/1 and do
%%      any necessary cleaning up. When it returns, the gen_server
%%      terminates with Reason. The return value is ignored.
%% @end
%%--------------------------------------------------------------------
-spec terminate(Reason :: any(), State :: record()) -> ok.
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc Convert process state when code is changed
%% @end
%%--------------------------------------------------------------------
-spec code_change(OldVsn :: any(), State :: record(), Extra :: any())
		 -> {ok, State :: record()}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
