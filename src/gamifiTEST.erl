%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @copyright 2014, Laura M. Castro
%%% @doc `gamifiTEST'
%%%      Gamification of unit testing and property-based testing
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see [http://www.gnu.org/licenses].
%%% @end
%%%-------------------------------------------------------------------
-module(gamifiTEST).

-include("gamifiTEST.hrl").

%% PUBLIC API
-export([test/1, check/1, coverage/1]).

%% Internal API
-export([measure_and_report/1]).

%%-------------------------------------------------------------------
%% @doc Invokes the wrapper for eUnit.
%%      Receives a module name (module must be compiled, and
%%      eUnit test module name_tests must exist and be compiled).
%% @end
%%-------------------------------------------------------------------
-spec test(Module :: atom()) -> ok.
test(Module) when is_atom(Module) ->
    gamifiTEST_template(eunit_wrapper, process, [Module]).

%%-------------------------------------------------------------------
%% @doc Invokes the wrapper for QuickCheck/Proper.
%%      Receives a test module name (test module must be compiled).
%% @end
%%-------------------------------------------------------------------
-spec check(Module :: atom()) -> ok.
check(Module) when is_atom(Module) ->
    gamifiTEST_template(pbt_wrapper, process, [Module]).

%%-------------------------------------------------------------------
%% @doc Invokes the wrapper for Cover/Smother.
%%      Receives a module name (module needs not be compiled).
%% @end
%%-------------------------------------------------------------------
-spec coverage(Module :: atom()) -> ok.
coverage(Module) when is_atom(Module) ->
    gamifiTEST_template(coverage_wrapper, process, [Module]).



%% Internal stuff

% @doc Gathers statistics and submits them.
%      This is an internal callback that gathers statistics from other parts of
%      the application (`gamifiTEST_brain') after test execution, and are then
%      submitted to the gamification server (via `gamifiTEST_io').
% @end
% @see gamifiTEST_brain
% @see gamifiTEST_io
-spec measure_and_report(Results :: term()) -> ok.
measure_and_report(Results) ->
    Gametrics = gamifiTEST_brain:measure(Results),
    ok = gamifiTEST_io:report(Gametrics).

% We use a template method for all gamifiTEST operations, since they have a
% common structure: execute test + measure + report.
-spec gamifiTEST_template(Module :: atom(),
			  Method :: atom(), Arguments :: [term()]) -> ok.
gamifiTEST_template(Module, Method, Arguments) ->
    Modules = [inets, crypto, asn1, public_key, ssl],
    Started = pre_hook(Modules),
    Results = erlang:apply(Module, Method, Arguments),
    measure_and_report(Results),
    post_hook(Started),
    ok.

-spec pre_hook(Modules :: [atom()]) -> [atom()].
pre_hook(Modules) ->
    AttemptedStart = [{M, application:start(M)} || M <- Modules],
    [M || {M, ok} <- AttemptedStart].

-spec post_hook(Modules :: [atom()]) -> ok.
post_hook(_Modules) ->
    ok.
    % [application:stop(M) || M <- Modules].
