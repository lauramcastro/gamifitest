%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @copyright 2014, Laura M. Castro
%%% @doc `gamifiTEST': `eunit_wrapper'
%%%      Gamification of unit testing and property-based testing
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see [http://www.gnu.org/licenses].
%%% @end
%%%-------------------------------------------------------------------
-module(eunit_wrapper).
-behaviour(test_wrapper).

-include("gamifiTEST.hrl").

-include_lib("eunit/include/eunit.hrl").

%% PUBLIC API (behaviour)
-export([process/1]).

%% @doc Wrapper for eUnit.
%%      Receives a module name (module must be compiled, and
%%      eUnit test module name`_tests' must exist and be compiled
%%      as well). It acts as a proxy of eUnit, delegating the
%%      actual execution of the tests, and listens for the statistical
%%      info, provided by a `eunit_collect' process.
%% @end
%% @see eunit_collect
-spec process(Module :: atom()) -> term().
process(Module) when is_atom(Module) ->
    register(?UNIT_DATA_RECEIVER, self()),
    eunit:test(Module, [{report,{eunit_collect,[]}}]), % Options could be {dir,"."}
    receive
	{?UNIT_DATA_SENDER, TestResults} ->
	    unregister(?UNIT_DATA_RECEIVER),
	    {eunit, TestResults}
    end.
