%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @copyright 2014, Laura M. Castro
%%% @doc `gamifiTEST'
%%%      Gamification of unit testing and property-based testing
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see [http://www.gnu.org/licenses].
%%% @end
%%%-------------------------------------------------------------------
-module(gamifiTEST_sup).

-behaviour(supervisor).

%% PUBLIC API
-export([start_link/0]).

%% Behaviour supervisor callbacks
-export([init/1]).

-define(SUPERVISOR, ?MODULE).

%% ===================================================================
%% Supervisor API
%% ===================================================================

% @doc Starts `gamifiTEST' supervisor.
%      Just like any other Erlang/OTP supervisor, this process will
%      oversee the execution of its children, and re-start them, in
%      case of failure (crash), according to the specified
%      restarting policy. In the case of gamifiTEST, children
%      processes (just one child actually, the gamifiTEST_daemon)
%      will be restarted at most 10 times in 60 minutes. If a
%      child process fails more often than that (more than once
%      every 6 minutes), this supervisor will give up and the
%      application will be shut down.
% @end
% @see gamifiTEST_daemon
-spec start_link() -> {ok, pid()} | ignore | {error, Reason :: string()}.
start_link() ->
    supervisor:start_link({local, ?SUPERVISOR}, ?MODULE, []).



%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc Whenever a supervisor is started using supervisor:start_link/[2,3],
%%      this function is called by the new process to find out about
%%      restart strategy, maximum restart frequency and child
%%      specifications.
%% @end
%%--------------------------------------------------------------------
-spec init(Args :: list()) -> {ok, {SupFlags :: tuple(), [ChildSpec :: list(tuple())]}}
				  | ignore | {error, Reason :: string()}.
init([]) ->
    RestartStrategy = one_for_one,
    MaxRestarts = 10,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Restart = permanent,
    Shutdown = 2000,
    Type = worker,

    Children = [ {Child, {Child, start_link, []},
                  Restart, Shutdown, Type, [Child]} || Child <- [gamifiTEST_daemon]],

    {ok, {SupFlags, Children}}.
