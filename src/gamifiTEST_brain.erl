%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @copyright 2014, Laura M. Castro
%%% @doc `gamifiTEST_brain'
%%%      Gamification of unit testing and property-based testing:
%%%      Module to extract gamification metrics from test data
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see [http://www.gnu.org/licenses].
%%% @end
%%%-------------------------------------------------------------------
-module(gamifiTEST_brain).

%% PUBLIC API
-export([measure/1]).

%%-------------------------------------------------------------------
%% @doc Determines new game achievements.
%%      After test execution, performed activities are examined to
%%      determine if new achievements have been made, according to
%%      existing game rules. This is information is returned in the
%%      form of a set of metrics.
%% @end
%%-------------------------------------------------------------------
-spec measure(Activity :: {Tool :: atom(),
			   Data :: term()}) -> Metrics :: term().
% (1) We will pattern-match on the kind of Data (representative -or not- of
%     the type of test that we have just run)
% (2) We [might] want to get some data from the REST server.
% (3) We produce some metrics.
measure({junit, Data}) ->
    io:format("[~p] receiving and measuring JUnit data: ~p~n", [?MODULE, Data]),
    AdditionalData = gamifiTEST_io:request(),
    {Data, AdditionalData};
measure({eunit, Data}) ->
    io:format("[~p] receiving and measuring eUnit data: ~p~n", [?MODULE, Data]),
    AdditionalData = gamifiTEST_io:request(),
    {Data, AdditionalData};
measure({eqc, Data}) ->
    io:format("[~p] receiving and measuring QuickCheck data: ~p~n", [?MODULE, Data]),
    AdditionalData = gamifiTEST_io:request(),
    {Data, AdditionalData};
measure({cover, RawData}) ->
    io:format("[~p] receiving and measuring cover data: ~p~n", [?MODULE, RawData]),
    AdditionalData = gamifiTEST_io:request(),
    {digest(RawData, []), AdditionalData};
measure(Other) ->
    io:format("[~p] unknown data received, not processing: ~p~n", [?MODULE, Other]),
    {[], []}.


%% Internal stuff

-spec digest(RawData :: list(tuple()), list()) -> {ok, list(tuple())}.
digest([], Acc) ->
    Acc;
digest([{{M,F,Args}, {Cov, 0}} | MoreData], Acc) when Cov =/= 0 ->
    ProcessedData = {printable({M,F,Args}), {percentage, 100}},
    digest(MoreData, [ProcessedData | Acc]);
digest([{{M,F,Args}, {Cov, NotCov}} | MoreData], Acc) ->
    ProcessedData = {printable({M,F,Args}), {percentage, NotCov / Cov}},
    digest(MoreData, [ProcessedData | Acc]);
digest([{{M,F,Args}, NCalls} | MoreData], Acc) ->
    ProcessedData = {printable({M,F,Args}), {ncalls, NCalls}},
    digest(MoreData, [ProcessedData | Acc]).

-spec printable(any()) -> string().
printable({M,F,Args}) ->
    atom_to_list(M) ++ ":" ++ atom_to_list(F) ++ "/" ++ integer_to_list(Args).
