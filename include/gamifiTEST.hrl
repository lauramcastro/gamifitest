%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro
%%% @doc gamifiTEST
%%%      Gamification of unit testing and property-based testing (header file).
%%% @copyright (C) 2014, Laura M. Castro
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%% 
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% 
%%% You should have received a copy of the GNU General Public License
%%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%% @end
%%%-------------------------------------------------------------------
-define(COVERAGE_TOOL, cover).

-define(PBT_TOOL, eqc).
-include_lib("eqc/include/eqc.hrl").
%-define(PBT_TOOL, proper).
%-include_lib("proper/include/proper.hrl").

-define(UNIT_DATA_RECEIVER, eunit_wrapper).
-define(UNIT_DATA_SENDER, eunit_collect).

-define(PBT_DATA_RECEIVER, pbt_wrapper).
-define(PBT_DATA_SENDER, pbt_collect).

-define(EXTERNAL_DATA_RECEIVER, external_wrapper).

-define(LOG_FILE, "/tmp/gamifiTEST.log").
