REPO       ?= gamifiTEST
TAG         = $(shell git describe --tags)
REVISION   ?= $(shell echo $(TAG) | sed -e 's/^$(REPO)-//')
VERSION    ?= $(shell echo $(REVISION) | tr - .)

REBAR=./rebar

all: compile

compile: deps
	@$(REBAR) compile

deps:
	@$(REBAR) get-deps

clean:
	rm -f current_counterexample*
	@$(REBAR) clean

distclean: clean
	rm -rf $(CURDIR)/deps
	rm -rf $(CURDIR)/doc

docs:
	@$(REBAR) doc

tests: eunit qc

eunit:
	@rm -rf .eunit
	@$(REBAR) eunit

qc:
	@rm -rf .qc
	@$(REBAR) qc
