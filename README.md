gamifiTEST
=========

Gamification of unit testing and property-based testing.

Dependencies
------------

To run the tool and the examples, you will need a working [Erlang/OTP](http://www.erlang.org/) installation, plus either [QuickCheck](http://www.quviq.com/) or [PropEr](http://proper.softlab.ntua.gr/) correctly installed and configured.

Also, the tool integrates with a gamification server via a REST API. This repository does not include such gamification server, but it can be used nonetheless.

Download & configuration
------------------------

Clone/download the tool repo.

Edit file `include/gamifiTEST.hrl` to set the PBT tool you wil be using (QuickCheck or PropEr), plus the path to your desired LOG_FILE.

Edition of `config.properties` should not be needed, but `ant.properties` should indeed be updated to point to the proper location of libraries such as JInterface (depends on your Erlang/OTP installation) or JUnit.

Once done, just run `make` to build the tool.

Running the examples
--------------------

### Erlang ###

There are some examples you can use in the `examples` folder.

After building, type `run` to enter an Erlang VM. Once within the EVM, start the application by typing:

    application:start(gamifiTEST).

Then type for instance:

    gamifiTEST:test(adder).
    gamifiTEST:check(adder).
    gamifiTEST:coverage(adder).

The tool will then, respectively:

1. Run EUnit tests (present in the same `examples` folder), PBT tests (likewise), and/or cover (using the aforementioned tests).
2. Collect different measures based on those executions.
3. Report those measures to a gamification server, if present.

### Java ###

There are some examples you can use in the `examples` folder.

After building, type `run-daemon` to start the application in daemon mode. You can verify this has been done properly by checking it is indeed running:

    ps xau | grep gamifiTEST

Also, a the LOG_FILE should be created (and empty for the time being).

    tail -f /tmp/gamifiTEST.log

To run the examples, just type `ant run`. The tool will then, respectively:

1. Run JUnit tests (present in the aforementioned `examples` folder).
2. Collect different measures based on those executions.
3. Print a log trace in the LOG_FILE.
4. Report those measures to a gamification server, if present.

Acknowledgements
---------------

This tool has been developed as part of the [GOAL](http://www.innterconectagoal.es/) research project.